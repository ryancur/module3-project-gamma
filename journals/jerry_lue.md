5/8/23 - Got familiarized with the app and ran docker containers. Brainstormed ideas and started wireframe models.
5/9/23 - More work on wireframe models fleshed out the mvp and what our stretch goals would be.
5/10/23 - Finished wireframe models and started mapping api endpoints
5/11/23 - Finished api endpoints and created issues
5/12/23 - Fast api tutorial
5/15/23 - Decided on non relational database and started making database in docker compose
5/16/23 - Made create account and log in log out functions
5/17/23 - Finished account crud functions
5/18/23 - started account create function
5/22/23 - finished activity delete and get functions
5/23/23 - finished activity crud
5/24/23 - started front end split off into individual doing activity form
5/25/23 - working on activity details
5/26/23 - activity details continued
5/30/23 - finished activity details and troubleshooting dependencies
5/31/23 - started working on activity update
6/1/23 - working on activity update and rerouting
6/5/23 - finished activity update and started cleaning up accounts front end
6/6/23 - remaking accounts into an icon in the nav and adding profile pic to backend and front. user testing with my sister senior UI/UX designer
6/7/23 - incorporating user testing feedback pages should rerout upon submission instead of reseting to blank
6/8/23 - making unit test
6/9/23 - finished about page and user testing and cleanup
