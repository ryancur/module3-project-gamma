5/8 - Cloned repo and ran docker containers, began brainstorming ideas for the project
5/9 - Worked on wireframe models, decided MVP functionality and stretch goals, picked color scheme for application
5/10 - Finished wireframe model and worked on our API endpoints
5/11 - Finished API endpoints and finished creating issues for our MVP functions
5/12 - Moved through FASTAPI tutorial and reviewed explorations to prepare for next week
5/15 - Selected MongoDB as our database and began structuring out docker-compose.yaml file.
5/16 - The group worked on creating accounts and login/logout , I began work on delete accounts
5/17 - Finished login/logout, I assisted on finishing updating accounts, Paul finished delete accounts function
5/18 - I drove today, Created activity models and and implemented a working create activity function, will edit datetime functionality.
5/22 - Edited the "get all activities" function to work, Finished the backend with by completing the update activities and get one activity
       functions.
5/23 - Implemented the calendar portion of the application and began working on frontend components. Also began work on editing the backend
       to change the "category" and "priority" fields of our activity models.
5/24 - Finished changing the backend to implement the changes to the activity model. Worked with Paul and got the Homepage of our
       application working by changing the routes for the frontend.
5/25 - Finished Homepage and added things to browser router, worked as a team on more frontend components
5/26 - Got activitieslist working and began studying redux.
5/29 - implemented redux into our application, setup our get and create activities in the redux store
5/30 - began working on activity details and did more research into implementing modals for our project
5/31 - implemented a new backend for our calendar, also managed to get our data to appear on the calendar
6/1 -  Separated tasks and events on our calendar and put tasks under the daypilot navigator.
6/2 - installed react-strap, worked on UpdateActivites and looking at the source code for our calendar.
6/3 - Fixed the login and signup modals to work with reactstrap , got the navbar to only appear when a user is logged in.
6/5 - Worked more on integrating our endpoints into the calendar functions, began changing the looks of things in CSS/reactstrap,
       got the create activity modal to show on the calendar.
6/6 - Worked on more calendar integration. Got the delete function working, Added an accordion to the calendar page to display
      tasks.
6/7 - Worked on the calendar trying to get create and update to function properly
6/8 - worked on the calendar some more, was not able to integrate the calendar with our backend, began troubleshooting bugs with
    our application.
6/9- Worked on trying to solve the the issues with refresh on our deployed page, was not able to solve, did cleanup for final
   submission
