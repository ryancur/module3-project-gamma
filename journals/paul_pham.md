5/8/2023 - Cloned the repository and created md's for everyone. Read the readme and followed instructions. Started ideas and wireframe models through excalidraw.

5/9/2023 - Continuted working on the wirefram models and determine the MVP and stretch goals

5/10/2023 - Finished the wirefram models and started mapping out the API endpoint design.

5/11/2023 - Worked on resumes for a bit and now going to finish up and finalize API endpoint design.

5/12/2023 - FastAPI tutorial for next week.

5/15/2023 - We are setting up the MongoDB Dockerfile and Accounts microservice.

5/16/2023 - Standup and continuing to debug an error.

5/17/2023 - Worked on API endpoints for deleteing and retreiving account.

5/18/2023 - Worked on creating activities models and routing them.

5/22/2023 - Worked on the API endpoints.

5/23/2023 - Worked on issues for GitLab for front-end.

5/24/2023 - Worked on Homepage for front-end.

5/25/2023 - Worked on activities list.

5/26/2023 - Worked on activities details.

5/30/2023 - Worked on bug for authentication when grabbing activity detail. Anyone could grab the activity detail if they had the ID of it regardless if logged in or not. Created new issues for the team to work on.

5/31/2023 - Worked on fix for auto-refreshing the page when logging in/out.

6/1/2023 - Troubleshooting individual bug. NodeJS and React would not auto-refresh the page for CSS and HTML.

6/2/2023 - Troubleshooting individual bug. NodeJS and React would not auto-refresh the page for CSS and HTML.

6/3/2023 - Worked on the delete activity function for the activity detail modal. Also fixed the bug for activity update having a gray dropdown still showing up even when you change to the update activity page when clicking Edit for activity detail.

6/5/2023 - Worked on the front end CSS/HTML and home page to make it look similar to our excalidraw.

6/6/2023 - Worked on the front end CSS/HTML. Made the forms uniform so it is the same format.

6/7/2023 - Worked on user feedback/testing. Relocated the sign in and login button. Fixed the navbar and adjusted the format of forms. Made some redirects.

6/8/2023 - Worked on unit tests for get an account.

6/9/2023 - Worked on Tests. Did ReadMe documentation, wireframes, api-design, ghi, integrations md files.
