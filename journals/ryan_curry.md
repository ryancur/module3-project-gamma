# Project Journal

Stand-up Questions:

1. What did you do yesterday?
2. What are you planning to do today?
3. Do you have any blockers/challenges to your progress?

NOTE: I wrote my journal on paper and then would input the entires at the end of the week.


## Journal

=====================

Date: 2023-06-09

Activities:

- troubleshot some routing issues with deployed app
- troubleshot issues with environment variables (needed help from seirs)
- general code cleanup

=====================

Date: 2023-06-08

Activities:

- worked on unit tests - attempted using a mock database
- wrote unit test for getting one activity
- wrote unit test for getting all activities with an empty list responds

=====================

Date: 2023-06-07

Activities:

- completed deployment pipeline - app deploying consistently from main
- tested the initial state of the app once deployed

=====================

Date: 2023-06-06

Activities:

- wrote first test case for CI pipeline
- cleaned up backend code - fixed flake8 issues
- completed CI pipeline

=====================

Date: 2023-06-05

Activities:

- worked on the CICD pipeline all day

=====================

Date: 2023-06-02

Activities:

- worked on activity update form and functionality
- added delete all activities associated with a user account when the user deletes their account
- troubleshot a bug with the category and priority enum types in the update activity modal with the team

=====================

Date: 2023-06-01

Activities:

- Andrew helped us troubleshoot our forms not clearing and our signup and login models not working
- forms clearing for update account and create activity
- worked on adding a form to the activity detail modal

=====================

Date: 2023-05-31

Activities:

- tried to debug signup and login modal pop-up issue
- worked on getting the form to clear when a successful event happens when updating a user account - got a hacky version to work - more to be done here

=====================

Date: 2023-05-30

Activities:

- worked on getting data to display in the ActivitiesList component
- worked on the ActivityDetail component - changed it to a modal that pops up when the detail button is clicked
- reviewed and tried to debug login and signup modals - not popping up when button is clicked
- reviewed calendar component code
- general code clean up

=====================

Date: 2023-05-26

Activities:

- moved Provider store to index.js
- tested jwtdown-react to see if we could use it instead of the redux store

=====================

Date: 2023-05-25

Activities:

- team worked on wiring up the frontend
- connected frontend auth to app
- auth connected to backend - tested through websocket

=====================

Date: 2023-05-24

Activities:

- implemented frontend auth with redux
- implemented web socket connection between frontend and backend
- added initial files for redux store on frontend
- updated package.json with needed dependencies for redux
- updated docker compose for frontend and websockets

=====================

Date: 2023-05-23

Activities:

- (backend) fixed create account 500 error stemming from session queries
- (backend) fixed logout when user deletes their account - deletes sessions
- created issues for frontend development in GitLab
- reviewed frontend docs and code

=====================

Date: 2023-05-22

Activities:

- helped troubleshoot and edit code to filter tasks by account ID
- merged finished activities code into main
- finished logout functionality when a user deletes their account

=====================

Date: 2023-05-18

Activities:

- cleaned up some endpoint code for accounts
- worked on auth for delete endpoint - logout functionality

=====================

Date: 2023-05-17

Activities:

- updated accounts endpoints
- worked on auth for delete endpoint

=====================

Date: 2023-05-16

Activities:

- ran into some bugs with the authentication
- resolved the 500 error when trying to create a new account (auth)
- auth endpoints working - need to do some more testing

=====================

Date: 2023-05-15

Activities:

- went through the MongoDB tutorial
- decided on the type of database we want to use - MongoDB
- started working on authentication

=====================

Date: 2023-05-12

Activities:

- went through the fastAPI tutorial from the exploration to prepare to start programming

=====================

Date: 2023-05-11

Activities:

- finished API design
- added issues on GitLab

=====================

Date: 2023-05-10

Activities:

- finished wireframes and started API endpoint design

=====================

Date: 2023-05-09

Activities:

- define MVP
- wireframes
- defined stretch goals

=====================

Date: 2023-05-08

Activities:

- created project repo
- ran docker containers
- started mock-ups
